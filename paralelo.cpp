#include <CL/cl.h>
#include <stdio.h>
#include <time.h> 
#include <omp.h>
 
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>

char leerFuentes(char *&src, const char *fileName)
{
    FILE *file = fopen(fileName, "rb");
    if (!file) {
        printf("Error al abrir el fichero '%s'\n", fileName);
        return 0;
    }

    if (fseek(file, 0, SEEK_END))  {
        printf("Error de posicionamiento en el '%s'\n", fileName);
        fclose(file);
        return 0;
    }

    long size = ftell(file);
    if (size == 0)  {
        printf("Error al calcular el tama�o del fichero '%s'\n", fileName);
        fclose(file);
        return 0;
    }

    rewind(file);
    src = (char *)malloc(sizeof(char) * size + 1);
    size_t res = fread(src, 1, sizeof(char) * size, file);
    if (res != sizeof(char) * size)   {
        printf("Error de lectura del fichero '%s'\n", fileName);
        fclose(file);
        free(src);
        return 0;
        }
    src[size] = '\0';
    fclose(file);
    return 1;
}

cl_int obtenerPlataformas(cl_platform_id *&platforms, cl_uint &num_platforms){
	cl_int err, i;
	err=clGetPlatformIDs(0, NULL, &num_platforms);
	if(err!=CL_SUCCESS){
		printf("Error al obtener plataforma\n");
		return err;
	}
	platforms=new cl_platform_id[num_platforms];
	err=clGetPlatformIDs(num_platforms, platforms, NULL);
	if(err!=CL_SUCCESS){
		printf("Error al obtener plataforma\n");
		return err;
	}
	for(i=0; i<num_platforms;i++){
		char nombre[1024];
		err=clGetPlatformInfo(platforms[i], CL_PLATFORM_NAME, 1023, nombre, NULL); 
		if(err!=CL_SUCCESS){
			printf("Error al obtenr informacion de plataforma\n");
			return err;
		}
		printf("%s\n", nombre);
	}
	return CL_SUCCESS;
}

cl_int ObtenerDispositivos(cl_platform_id platform, cl_device_type device_type, cl_device_id *&device_ids, 
			cl_uint &num_devices){
	cl_int err, i;
	err=clGetDeviceIDs(platform, device_type, 0, NULL, &num_devices);
	if(err!= CL_SUCCESS){
		printf("Error al obtener numero de dispositivos\n");
		return err;
	}
	device_ids= new cl_device_id[num_devices];
	err=clGetDeviceIDs(platform, device_type, num_devices, device_ids, &num_devices);
	if(err!=CL_SUCCESS){
		printf("error al obtener los dipositivos\n");
		return err;
	}
	for(i=0; i<num_devices; i++){
		char nombre[1024];
		err=clGetDeviceInfo(device_ids[i], CL_DEVICE_NAME, 1023, nombre, NULL);
		if(err!=CL_SUCCESS){
			printf("Error al obtener informacion de dispositovos\n");
			return err;
		}
		printf("%s\n", nombre);
	}
	return CL_SUCCESS;
}

cl_int CrearContexto (cl_platform_id platform, cl_device_id *devices, cl_uint num_devices, cl_context &contexto){
	cl_context_properties prop[3];
	cl_int err;
	prop[0]=CL_CONTEXT_PLATFORM;
	prop[1]=(cl_context_properties)platform;
	prop[2]=0;
	contexto=clCreateContext(prop, num_devices, devices, NULL, NULL, &err);
	if(err!= CL_SUCCESS){
               	printf("Error al crear contexto\n");
                return err;
        }
	return CL_SUCCESS;
}

cl_int CrearColas (cl_context contexto, cl_device_id device_id, cl_command_queue_properties que_prop, cl_uint que_size, cl_command_queue &cola){
	cl_int err;
	int num_prop=1, desp=0;
	if(que_prop!=0 ) num_prop+=2;
	if(que_size>0) num_prop+=2;
	cl_command_queue_properties prop[num_prop];
	if(que_prop!=0){
		prop[0]=CL_QUEUE_PROPERTIES;
		prop[1]=que_prop;
		desp+=2;
	}
	if(que_size>0){
		prop[desp]=CL_QUEUE_SIZE;
		prop[desp+1]=que_size;
	}
	prop[num_prop-1]=0;
	cola=clCreateCommandQueueWithProperties(contexto, device_id, prop, &err);

	if(err!= CL_SUCCESS){
                printf("Error al crear la cola\n");
                return err;
        }
	return CL_SUCCESS;
}

cl_int CrearPrograma(cl_program &program, cl_context context, cl_uint num_devices, const cl_device_id *device_list, const char *options, const char *fichero){
	cl_int err;
	char *fuente;
	if(!leerFuentes(fuente, fichero))
		return -1;
	program=clCreateProgramWithSource(context, 1, (const char **)&fuente, NULL, &err);

	if(err!= CL_SUCCESS){
                printf("Error al crear el programa\n");
                return err;
        }
	err=clBuildProgram(program, num_devices, device_list, options, NULL, NULL);

	if(err!=CL_SUCCESS){
		printf("Error al compilar el programa\n");
		if(err==CL_BUILD_PROGRAM_FAILURE){
			char *buffer[1024];
			clGetProgramBuildInfo(program, device_list[0], CL_PROGRAM_BUILD_LOG, 1023, buffer, NULL);
 			printf("%s\n", buffer);
		}
		return err;
	}
	return CL_SUCCESS;
}

cl_int CrearKernel (cl_kernel &kernel, cl_program program , const char *kernel_name){
	cl_int err;
	kernel=clCreateKernel(program, kernel_name, &err);
	if(err!= CL_SUCCESS){
                printf("Error al crear kernel\n");
                return err;
        }	
	return CL_SUCCESS;
}

cl_int CrearBuffer(cl_context context, cl_mem_flags flags, size_t size, void *host_ptr, cl_mem &buffer){
	cl_int err;
	buffer=clCreateBuffer(context, flags, size, host_ptr, &err);	

	if(err!= CL_SUCCESS){
                printf("Error al crear buffer\n");
                return err;
        }
	return CL_SUCCESS;
}

cl_int AsignarParametro(cl_kernel kernel, cl_uint arg_index, size_t arg_size, const void *arg_value){
	cl_int err;
	err=clSetKernelArg(kernel, arg_index, arg_size, arg_value);
	if(err!= CL_SUCCESS){
                printf("Error al asignar parametro %d\n", arg_index);
                return err;
        }
	return CL_SUCCESS;
}

cl_int EnviarBuffer(cl_command_queue command_queue, cl_mem buffer, cl_bool blocking_write, size_t offset, 
	size_t cb, const void *ptr, cl_uint num_events, const cl_event *event_wait_list, cl_event &event){
	cl_int err;
	err=clEnqueueWriteBuffer(command_queue, buffer, blocking_write, offset, cb, ptr, num_events, event_wait_list, &event);
	 if(err!= CL_SUCCESS){
                printf("Error al enviar el buffer\n");
                return err;
        }
	return CL_SUCCESS;
} 

cl_int RecibirBuffer(cl_command_queue command_queue, cl_mem buffer, cl_bool blocking_read, size_t offset, size_t cb, void *ptr, cl_uint num_events, const cl_event *event_wait_list, cl_event &event){
	
	cl_int err;
        err=clEnqueueReadBuffer(command_queue,	buffer,	blocking_read,	offset,	cb, ptr, num_events, event_wait_list, &event);
         if(err!= CL_SUCCESS){
                printf("Error al enviar el buffer\n");
                return err;
        }
        return CL_SUCCESS;
}

cl_int EjecutarKernel(cl_command_queue command_queue, cl_kernel kernel, cl_uint work_dim, const size_t *global_work_offset, const size_t *global_work_size, size_t *local_work_size,  cl_uint num_events, const cl_event *event_wait_list, cl_event &event){
	cl_int err;
	err=clEnqueueNDRangeKernel(command_queue, kernel, work_dim, global_work_offset,
	global_work_size, local_work_size, num_events, event_wait_list, &event);
	if(err!= CL_SUCCESS){
         	printf("Error al enviar el buffer\n");
                return err;
        }
	return CL_SUCCESS;
}

cl_int ObtenerTiempoEjecucionEvento(cl_event event, cl_ulong &tiempo){
	cl_int err;
	cl_ulong t_ini, t_fin;
	err=clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &t_ini, NULL);
	if(err!= CL_SUCCESS){
               printf("Error al enviar el buffer\n");
	       return err;
        }

	err=clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &t_fin,	NULL);
        if(err!= CL_SUCCESS){
               printf("Error al enviar el buffer\n");
               return err;
        }
	printf("tiempo ini %u, tiempo fin %u\n", t_ini, t_fin);
	tiempo=t_fin-t_ini;
        return CL_SUCCESS;
}

cl_int ObtenerTiempoEjecucionEntreEventos(cl_event event_ini, cl_event event_fin, cl_ulong &tiempo){
        cl_int err;
        cl_ulong t_ini,	t_fin;
        err=clGetEventProfilingInfo(event_ini, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &t_ini, NULL);
        if(err!= CL_SUCCESS){
               printf("Error al enviar el buffer\n");
               return err;
        }
        return CL_SUCCESS;
        err=clGetEventProfilingInfo(event_fin, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &t_fin, NULL);
        if(err!= CL_SUCCESS){
               printf("Error al enviar el buffer\n");
               return err;
        }
        tiempo=t_fin-t_ini;
        return CL_SUCCESS;
}


void initialize(cl_char *m,int t){
	int i, j;
	// Se genera la matriz con valores 0 y 1, con la misma probabilidad
	for(i=0;i<t*t;i++)
		m[i] = (char) ((1.*rand()/RAND_MAX)*2.);
}

void escribir(cl_char *m, int t){
	int i, j;
	for (i = 0; i < t; i++)
	{
		for (j = 0; j < t; j++)
			printf("%d ",m[i*t+j]);
		printf("\n");
	}
	printf("\n");
}

/*
c
c     mseconds - returns elapsed milliseconds since Jan 1st, 1970.
c
*/
long long mseconds(){
  struct timeval t;
  gettimeofday(&t, NULL);
  return t.tv_sec*1000 + t.tv_usec/1000;
}

int ObtenerParametros(int argc, char *argv[], int *debug, size_t *num_work_items) {
	*debug=0;
	*num_work_items=0;
	int aux=0;
	if (argc<2 || argc>5)
		return 0;
	switch (argc) {
		case 3: // -d o error
			if (argv[2][0]!='-' || argv[2][1]!='d')
				return 0;
			else
				*debug=1;
			return 1;
		case 4: // -w num_work_items o error
			if (argv[2][0]!='-' || argv[2][1]!='w')
				return 0;
			else {
				*num_work_items = atoi(argv[3]);
				if (*num_work_items<=0)
					return 0;
			}
			return 1;
		case 5: // -w num_work_items -d o -d -w num_work_items o error
			if (argv[2][0]=='-' && argv[2][1]=='w') { // -w num_work_items -d
				*num_work_items = atoi(argv[3]);
				aux=1;
				if (*num_work_items<=0)
					return 0;
				if (argv[4][0]=='-' && argv[4][1]=='d')
					*debug=1;
				else
					return 0;
			}
			else if (argv[3][0]=='-' && argv[3][1]=='w') { // -d -w num_work_items
				*num_work_items=atoi(argv[4]);
				aux=1;
				if (*num_work_items<=0)
					return 0;
				if (argv[2][0]=='-' && argv[2][1]=='d')
					*debug=1;
				else
					return 0;
			}
			else // error
				return 0;
			return 1;
	}
	return 1;
}

void copiarMatriz(cl_char *inicio, cl_char *destino, cl_int num_elem){

	int i, j;
	for(i = 0; i < num_elem; i++)
		for(j = 0; j < num_elem; j++)
			destino[i*num_elem+j] = inicio[i*num_elem+j];
}

int main (int argc, char *argv[]){		
	cl_context contexto;
	cl_platform_id *platforms;
	cl_uint num_platforms;
	obtenerPlataformas(platforms, num_platforms);
	cl_uint num_dispositivos;
	cl_device_id *dispositivos;	
	ObtenerDispositivos(platforms[0], CL_DEVICE_TYPE_ALL, dispositivos, num_dispositivos);

	CrearContexto (platforms[0], dispositivos, num_dispositivos, contexto);
	cl_command_queue cola;
	CrearColas (contexto, dispositivos[0], CL_QUEUE_PROFILING_ENABLE, 0, cola);
	cl_program program;
	
	CrearPrograma(program, contexto, num_dispositivos, dispositivos, NULL, "programa.cl");
	cl_kernel kernel;
	CrearKernel (kernel, program, "calculo");

	//PARTE PROYECTO
	size_t num_work_items;
	int debug=0;

	if (!ObtenerParametros(argc, argv, &debug, &num_work_items)) {
		printf("Número de parámetros erróneo\nEl formato correcto es principal fichEntrada [-d] [-w num_work_items]\n");
		return 0;
	}

	int vec, it, i, iter, cuantos, semilla, vecmax;
	long long ti,tf,tt=0; // tt acumulará los tiempos parciales de todos los experimentos realizados
	cl_char *a, *aux;
	FILE *f;
	cl_mem buffer_in, buffer_out;
	cl_int num_elem;
	
	// Se leen los datos de los experimentos
	f=fopen(argv[1],"r");
	fscanf(f, "%d",&cuantos);				  // Número de experimentos a realizar

	for(i=0;i<cuantos;i++){

		fscanf(f, "%d",&num_elem);                   // El primer argumento es el tamano del tablero (cuadrado)
		fscanf(f, "%d",&semilla);             // El segundo una semilla para la generacion aleatoria de los datos
		fscanf(f, "%d",&vecmax);              // El tercero es el tamano máximo de la vecindad
		fscanf(f, "%d",&iter);         // El cuarto el número de iteraciones a generar
		srand(semilla);

		a = (cl_char *) calloc(sizeof(cl_char),num_elem*num_elem);
		aux  = (cl_char *) calloc(sizeof(cl_char),num_elem*num_elem);
		initialize(a,num_elem);

		CrearBuffer(contexto, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, num_elem*num_elem*sizeof(cl_char), a, buffer_in);
		CrearBuffer(contexto, CL_MEM_WRITE_ONLY |CL_MEM_COPY_HOST_PTR, num_elem*num_elem*sizeof(cl_char), aux, buffer_out); 

		vec = 1;
		
		AsignarParametro(kernel, 0, sizeof(cl_mem), &buffer_in);
		AsignarParametro(kernel, 1, sizeof(cl_mem), &buffer_out);
		AsignarParametro(kernel, 2, sizeof(cl_int), &num_elem);
		AsignarParametro(kernel, 3, sizeof(cl_int), &vec );
		cl_event evento, evento_in, evento_out;

		// Si está en modo debug, se escribe el tablero origen
		if (debug){
			printf("Tablero inicial del experimento %d:\n", i); escribir(a,num_elem);
		}
		ti=mseconds(); 

		copiarMatriz(a, aux, num_elem); 

		for(it = 0; it < iter; it++){				

			AsignarParametro(kernel, 3, sizeof(cl_int), &vec);
			EnviarBuffer(cola, buffer_in, false, 0, num_elem*num_elem*sizeof(cl_char), a, 0, NULL, evento_in);	 
			EjecutarKernel(cola, kernel, 1, NULL, &num_work_items, NULL, 0, NULL, evento);
			RecibirBuffer(cola, buffer_out, false, 0, num_elem*num_elem*sizeof(cl_char), aux, 0, NULL, evento_out);

			clFinish(cola);

			if(vec == vecmax) vec = 1;
			else vec++;
			copiarMatriz(aux, a, num_elem);
		}	

		tf=mseconds(); 
		tt+=tf-ti;

		// Si está en modo debug, se escribe el tablero resultado
		if (debug) {
			printf("Tiempo del experimento %d: %Ld ms\n", i, tf-ti);
			printf("Resultado del experimento %d:\n", i); 
			escribir(a,num_elem);
		}	

	}
	printf("Tiempo total de %d experimentos:  %Ld ms\n", cuantos, tt);
	fclose(f);

	free(a);
	free(aux);

	return 1;
}

