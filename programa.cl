int calcularVecis (__global char *input, int i, int j, int vec, int num_elem){

	int x, y, xmin, ymin, xmax, ymax, mates = 0;
	
	if((xmin = i - vec) < 0) xmin = 0;
	if((ymin = j - vec) < 0) ymin = 0;
	if((xmax = i + vec) >= num_elem) xmax = num_elem-1;
	if((ymax = j + vec) >= num_elem) ymax = num_elem-1;

	for(x = xmin; x <= xmax; x++)
		for(y = ymin; y <= ymax; y++)
			if(input[x*num_elem+y] == 1)
				if(!(x == i && y == j))
					mates++;					
	return mates;
}

__kernel void calculo(__global char *input, __global char *output, __const int num_elem, int vec){

	size_t id = get_global_id(0);
	int work_items = get_global_size(0);
	int i, j, k, indice;
	int ini = id * num_elem * num_elem / work_items;
	int fin = (id + 1) * num_elem * num_elem / work_items;
	int mates;
	int estado = -1;

	for(k = ini; k < fin; k++){
		i = k / num_elem;
		j = k % num_elem;
		mates = calcularVecis(input, i, j, vec, num_elem);
		indice = i*num_elem+j;

		if(input[indice] == 1){
			if(mates < 2)				estado = 0;
			if(mates > vec+2) 			estado = 0;
		}else
			if(mates >= 3 && mates <= vec+2)	estado = 1;
			
		if(estado == -1)
			output[indice] = input[indice];	
		else{
			output[indice] = estado;
			estado = -1;
		}
	}
}
